<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin | {{ $menu }}</title>
{{--    <link rel="shortcut icon" type="image/x-icon" href="#">--}}
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datepicker/datepicker3.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/all.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/flat/blue.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/summernote/summernote.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <style>
        .btn{
            /*padding: 7px 17px!important;*/
        }
        .select2-container .select2-selection--single {
            height: 34px !important;
        }
    </style>

    <!-- CSS FOR ROBOCROP IMAGES -->
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/robocrop/css/style.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/robocrop/css/demo.css')}}">

</head>
<body class="hold-transition skin-blue sidebar-mini" id="bodyid">
<div class="wrapper">
    <header class="main-header">
        <a href="{{ url('admin/dashboard') }}" class="logo">
            <span class="logo-mini"><b>A</b>D</span>
            <span class="logo-lg"><b>Admin</b> </span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
{{--                            <img src="{{url('assets/dist/img/default-user.png')}}" class="user-image" alt="User Image">--}}
                            <img src="{{'http://54.169.196.24/bpacademy/assets/dist/img/default-user.png'}}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ $user = Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">

                            <li class="user-header">
                                <img src="{{'http://54.169.196.24/bpacademy/assets/dist/img/default-user.png'}}" class="img-circle" alt="User Image">
                                <p>
                                    {{ $user = Auth::user()->name }} <br>
                                    <small></small>
                                </p>
                            </li>

                            <li class="user-footer">
                                <div class="pull-right" style="margin-right: 85px;">
                                    <a href="{{ url('admin/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="@if($menu=='Dashboard') active  @endif">
                    <a href="{{ url('admin/dashboard') }}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li class="@if(isset($menu) && $menu=='Course') active @endif">
                    <a href="{{ url('admin/course') }}"><i class="fa fa-th"></i><span> Course Management</span></a>
                </li>

                <li class="@if(isset($menu) && $menu=='Student') active @endif">
                    <a href="{{ url('admin/student') }}"><i class="fa fa-th"></i><span> Student Management</span></a>
                </li>
            </ul>
        </section>
    </aside>

    @yield('content')

    <footer class="main-footer">
        <strong>Admin</strong>
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
        var previous_val = null;
        $(".select2").select2();
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
        $('#example1').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });
        $('#example2').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });
        // $.fn.dataTable.ext.errMode = 'none';
    });
</script>
@yield('jquery')
<script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
{{--<script src="{{ URL::asset('assets/plugins/morris/morris.min.js')}}"></script>--}}
<script src="{{ URL::asset('assets/plugins/chartjs/Chart.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/knob/jquery.knob.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/fastclick/fastclick.js')}}"></script>
<script src="{{ URL::asset('assets/dist/js/app.min.js')}}"></script>
{{--<script src="{{ URL::asset('assets/dist/js/pages/dashboard.js')}}"></script>--}}
{{--<script src="{{ URL::asset('assets/dist/js/pages/dashboard3.js')}}"></script>--}}
<script src="{{ URL::asset('assets/dist/js/demo.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/summernote/summernote.min.js')}}"></script>

<!--******************JS FOR ROBOCROP IMAGES******************-->
<script src="{{ URL::asset('assets/dist/robocrop/js/robocrop.demo.js')}}"></script>
<script src="{{ URL::asset('assets/dist/robocrop/js/demo.js')}}"></script>

<!-- script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ckeditor/ckfinder/ckfinder.js')}}"></script -->

<script type="text/javascript">

    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    $('#summernote').summernote({
        height: 250,
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                for(var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });

    function search_filters(event){
        var x = event.keyCode;
        if (x == 13) {  // 13 is the Enter key
            $("#searchbtn").click();
        }
    }
</script>

</body>
</html>
