{!! Form::hidden('redirects_to', URL::previous()) !!}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title">Name<span class="text-red"> *</span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="price">Price<span class="text-red"> *</span></label>
    <div class="col-sm-5">
        {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
        @if ($errors->has('price'))
            <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="price">Duration<span class="text-red"> *</span></label>
    <div class="col-sm-5">
        {!! Form::text('duration', null, ['class' => 'form-control', 'placeholder' => 'Duration']) !!}
        @if ($errors->has('duration'))
            <span class="help-block">
                <strong>{{ $errors->first('duration') }}</strong>
            </span>
        @endif
    </div>
</div>
