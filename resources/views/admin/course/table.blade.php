
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="5%">Id</th>
        <th width="45%">Name</th>
        <th width="20%">Price</th>
        <th width="20%">Duration</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $count = 0;?>
    @foreach ($course as $list)
        <?php $count++; ?>
        <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $list['id'] }}" />
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td>{{ $list['id'] }}</td>
            <td>{{ $list['name'] }}</td>
            <td>{{ $list['price'] }}</td>
            <td>{{ $list['duration'] }}</td>
            <td>
                <div class="btn-group-horizontal">
                    <a href="{{ url('admin/course/'.$list->id.'/edit') }}"> <button class="btn btn-success" type="button" style="margin-bottom: 5px"><i class="fa fa-edit"></i></button></a>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}" style="margin-bottom: 5px"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>

        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
            {{ Form::open(array('url' => 'admin/course/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete Course</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this Course?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
{{--                        <a href="{{ url('admin/course/'.$list['id']) }}" data-method="delete" name="delete_course">--}}
                            <button type="submit" class="btn btn-outline">Delete</button>
{{--                        </a>--}}
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            {{ Form::close() }}
        </div>
    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $course])</div>
<input type="hidden" name="count" id="count" value="<?php echo $count;?>" />