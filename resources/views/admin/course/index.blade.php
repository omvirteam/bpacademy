@extends('admin.layouts.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li class="active"><a href="#"><i class="fa fa-dashboard"></i>Course</a></li>
            </ol>

            <br>
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            <div class="box">
                <div class="box-header">
                    <div class="col-md-5 col-sm-7">
                        {!! Form::open(['url' => url('admin/course'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-7 col-xs-12">
                                <input  class="form-control" autocomplete="off" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search" onkeyup="search_filters(event)">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-5 col-xs-12">
                                <input type="submit" class="btn btn-primary pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7 col-sm-5">
                        <h3 class="box-title" style="float:right;">
                            <h3 class="box-title pull-right"><a href="{{ url('admin/course/create/') }}" ><button class="btn btn-primary margin" type="button">Add Course</button></a></h3>
                        </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.course.table')
                </div>
            </div>
        </section>

    </div>
@endsection
<script src="{{ URL::asset('assets/jquery.js')}}"></script>
