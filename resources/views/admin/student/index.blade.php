@extends('admin.layouts.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li class="active"><a href="#"><i class="fa fa-dashboard"></i>Student</a></li>
            </ol>

            <br>
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            <div class="box">
                <div class="box-header">
                    <div class="col-md-5 col-sm-7">
                        {!! Form::open(['url' => url('admin/student'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-7 col-xs-12">
                                <input  class="form-control" autocomplete="off" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search" onkeyup="search_filters(event)">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-5 col-xs-12">
                                <input type="submit" class="btn btn-primary pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7 col-sm-5">
                        <h3 class="box-title" style="float:right;">
                            <h3 class="box-title pull-right"><a href="{{ URL::to('admin/downloadExcel/') }}" ><button class="btn btn-primary margin" type="button">Export Student Data</button></a></h3>
                        </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive" id="itemlist">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Course</th>
                            <th>Price</th>
                            <th>Duration</th>
                            <th>Interested in Frenchisee</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = 0;?>
                        @foreach ($student as $list)
                            <?php $count++; ?>
                            <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $list['id'] }}" />
                            <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
                                <td>{{ $list['id'] }}</td>
                                <td>{{ $list['name'] }}</td>
                                <td>{{ $list['phone'] }}</td>
                                <td>{{ $list['email'] }}</td>
                                <td>{{ $list['address'] }}</td>
                                <td>{{ $list['course_name'] }}</td>
                                <td>{{ $list['price'] }}</td>
                                <td>{{ $list['duration'] }}</td>
                                <td>{{ $list['interested_in_frenchisee'] }}</td>
                                <td>
                                    <div class="btn-group-horizontal">
                                        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}" style="margin-bottom: 5px"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>

                            <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
                                {{ Form::open(array('url' => 'admin/student/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Delete Student</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to delete this Student?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline">Delete</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                {{ Form::close() }}
                            </div>
                        @endforeach
                    </table>
                    <div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $student])</div>
                    <input type="hidden" name="count" id="count" value="<?php echo $count;?>" />
                </div>
            </div>
        </section>

    </div>
@endsection
<script src="{{ URL::asset('assets/jquery.js')}}"></script>
