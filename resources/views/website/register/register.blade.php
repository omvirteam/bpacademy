<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register</title>
{{--    <link rel="shortcut icon" type="image/x-icon" href="#">--}}
<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/AdminLTE.min.css') }} ">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/square/blue.css') }}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin: 4% auto;">
    <div class="login-logo">
        <div>
            <img src="#" alt="" style="width: 200px;">
        </div>
        <a href="{{ URL::to('/') }}"><b>Registration</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Student Register</p>

        @include("admin/error")

        <form class="" role="form" method="POST" action="{{ url('store_register') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                <input type="text" name="name" autocomplete="off" class="form-control" placeholder="Full name">
            </div>

            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} has-feedback">
                <input type="text" name="address" autocomplete="off" class="form-control" placeholder="Address">
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} has-feedback">
                <input type="text" name="phone" autocomplete="off" class="form-control" placeholder="Phone">
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                <input type="text" name="email" autocomplete="off" class="form-control" placeholder="Email">
            </div>

            <div class="form-group{{ $errors->has('profession') ? ' has-error' : '' }} has-feedback">
                {!! Form::select('profession', ['0' => ' Select Profession', '1' => 'Business', '2' => 'Student', '3' => 'Housewife'], 0, ['class' => 'select2 form-control', 'style' => 'width: 100%','id'=>'profession',]) !!}
            </div>

            <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }} has-feedback">
                {!! Form::select('course_id', $course, null, ['class' => 'select2 form-control', 'style' => 'width: 100%','id'=>'course_id','onchange' => 'coursetype()']) !!}
            </div>

            <div class="form-group has-feedback" id="price_block" style="display: none;">
               <div class="col-md-4"  style="padding-left: 5px;">
                   <label class="control-label">Price: <span id="price_label"></span></label>
               </div>
                <div class="col-md-6">
                    <label class="control-label">Duration: <span id="duration_label"></span></label>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="interested_in_frenchisee"> Interested In Frenchisee
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
            </div>
        </form>

        <div class="box box-primary direct-chat direct-chat-primary" style="margin-top: 20px;">
            <div class="box-header with-border">
                <h3 class="box-title" style="font-size: 17px;">We Are Happy To Support You!</h3>
            </div>
            <div class="box-body">
{{--                <div class="direct-chat-messages" style="height: 180px;">--}}
                    <div class="direct-chat-msg right">
                        <div class="direct-chat-text" style="margin-right: 1px;">
                            Contact : 9437074651 <br>
                            WhatsApp : <a href="https://wa.me/919437074651" class="text-aqua" target="_blank">https://wa.me/919437074651</a> <br>
                            Email : bimal01@gmail.com <br>

                        </div>
                    </div>
{{--                </div>--}}
            </div>
        </div>

    </div>
</div>

<!-- jQuery 2.2.0 -->
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ URL::asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<script>
    function coursetype(){

        var cid = document.getElementById('course_id').value;
        if(cid != ''){
            $.ajax({
                url: '{{ url('get_course_price') }}',
                data:{id: cid},
                error:function(){
                },
                success: function(result){

                    document.getElementById('price_block').style.display = 'block';
                    document.getElementById('price_label').innerHTML = result[0];
                    document.getElementById('duration_label').innerHTML = result[1];
                }
            });
        }else{
            document.getElementById('price_block').style.display = 'none';
        }

    }
</script>
</body>
</html>
