-- Avinash : 2020-06-18 10:50 AM
ALTER TABLE `students` ADD `profession` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '1 = Business, 2 = Student, 3 = Housewife' AFTER `address`;