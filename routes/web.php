<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin','admin/home'], function()
{
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::auth();
    Route::get('dashboard', 'admin\DashboardController@index');
    Route::get('/', function () {
        return redirect('admin/dashboard');
    });

    //course
    Route::resource('course', 'admin\CourseController');

    //student
    Route::get('downloadExcel', 'admin\StudentController@downloadExcel');
    Route::resource('student', 'admin\StudentController');

    Auth::routes();
});

//register
Route::get('/', 'website\FrontController@index');
Route::post('store_register', 'website\FrontController@store_register');
Route::get('get_course_price', 'website\FrontController@get_course_price');
//Route::get('/', function () {
//    return view('welcome');
//});
//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');
