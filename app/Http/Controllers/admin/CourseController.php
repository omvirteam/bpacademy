<?php

namespace App\Http\Controllers\admin;

use App\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data['menu']="Course";
        if(isset($request['search']) && $request['search'] != '')
        {
            $search = $request['search'];
            $data['course'] = Course::where(function ($query) use ($search) {
                $query->orWhere('name','like','%'.$search.'%' );
                $query->orWhere('price','like','%'.$search.'%' );
                $query->orWhere('duration','like','%'.$search.'%' );
            })->Paginate($this->pagination);
            $data['search']=$request['search'];
        }
        else
        {
            $data['course']= Course::Paginate($this->pagination);
        }

        return view('admin.course.index',$data);
    }

    public function create()
    {
        $data['menu']="Course";
        return view('admin.course.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'duration' => 'required',
        ]);

        $input = $request->all();

        $user = Auth::user();
        $input['created_by'] = $user['id'];

        Course::create($input);

        \Session::flash('success', 'Course has been inserted successfully!');
        return redirect('admin/course');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu']="Course";
        $data['course'] = Course::findOrFail($id);
        return view('admin.course.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'duration' => 'required',
        ]);

        $course = Course::findOrFail($id);
        $input = $request->all();

        $user = Auth::user();
        $input['modified_by'] = $user['id'];

        $course->update($input);
        \Session::flash('success', 'Course has been Updated successfully!');
        return redirect('admin/course');
    }

    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $course->delete();
        \Session::flash('danger','Course has been deleted successfully!');
        return redirect('admin/course');
    }
}
