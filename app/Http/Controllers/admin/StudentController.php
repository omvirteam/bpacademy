<?php

namespace App\Http\Controllers\admin;

use App\Exports\StudentsExport;
use App\Http\Controllers\Controller;
use App\Student;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data['menu']="Student";
        if(isset($request['search']) && $request['search'] != '')
        {
            $search = $request['search'];
            $data['student'] = Student::where(function ($query) use ($search) {
                $query->orWhere('name','like','%'.$search.'%' );
                $query->orWhere('price','like','%'.$search.'%' );
                $query->orWhere('duration','like','%'.$search.'%' );
                $query->orWhere('course_name','like','%'.$search.'%' );
            })->Paginate($this->pagination);
            $data['search']=$request['search'];
        }
        else
        {
            $data['student']= Student::Paginate($this->pagination);
        }

        return view('admin.student.index',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $student = Student::findOrFail($id);
        $student->delete();
        \Session::flash('danger','Student has been deleted successfully!');
        return redirect('admin/student');
    }

    public function downloadExcel(){

        return Excel::download(new StudentsExport, 'students.xlsx');

    }
}
