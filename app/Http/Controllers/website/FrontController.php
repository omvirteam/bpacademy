<?php

namespace App\Http\Controllers\website;

use App\Course;
use App\Http\Controllers\Controller;
use App\Student;
use Illuminate\Http\Request;

class FrontController extends Controller
{

    public function index()
    {
        $data['course'] = Course::pluck('name','id')->prepend('Select Course','')->all();
        return view('website.register.register',$data);
    }

    public function store_register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'course_id' => 'required',
            'email' => 'required',
        ]);

        $input = $request->all();
        $input['interested_in_frenchisee'] = isset($request['interested_in_frenchisee'])?'yes':'no';

        $course = Course::where('id',$request['course_id'])->first();
        $input['course_name'] = $course['name'];
        $input['price'] = $course['price'];
        $input['duration'] = $course['duration'];

        Student::create($input);

        \Session::flash('success', 'Registration successfully!');
        return redirect('/');

    }

    public function get_course_price(Request $request){

        $course = Course::where('id',$request['id'])->first();
        $price = $course['price'];
        $duration = $course['duration'];
        return [$price,$duration];
    }
}
