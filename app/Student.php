<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name','phone','email','address','profession','course_id','course_name','price','duration','interested_in_frenchisee',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
